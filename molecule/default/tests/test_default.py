import os
import yaml

from os.path import expanduser
from lxml import etree
from jnpr.junos import Device

import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')

home = expanduser("~")


venv = open(home + "/.cache/molecule/ics-ans-role-junos-interfaces/default/inventory/ansible_inventory.yml")
parse_yaml = yaml.load(venv, Loader=yaml.FullLoader)
switch = parse_yaml['all']['hosts']['junos-interfaces-default']['ansible_host']
switch_port = parse_yaml['all']['hosts']['junos-interfaces-default']["ansible_port"]
switch_key = parse_yaml['all']['hosts']['junos-interfaces-default']["ansible_private_key_file"]
switch_user = parse_yaml['all']['hosts']['junos-interfaces-default']["ansible_user"]

with Device(host=switch, user=switch_user, ssh_private_key_file=switch_key, port=switch_port) as dev:
    data = dev.rpc.get_config()
    config = etree.tostring(data, encoding='unicode', pretty_print=True)


def test_config(host):
    assert "mgmt" in config
