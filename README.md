# ics-ans-role-junos-interfaces

Ansible role to install junos-interfaces.

## Role Variables

```yaml
# set VLANS
vlans:
  - name: clients
    vlanid: 3000
  - name: mobile
    vlanid: 4002
  - name: mgmt
    vlanid: 4094

# set interfaces
interfaces:
  - xe-0/0/0:0
  - xe-0/0/0:1
  - xe-0/0/0:2
  - xe-0/0/1:0
  - xe-0/0/1:1
  - xe-0/0/1:2
  - xe-0/0/2:0
  - xe-0/0/2:1
  - xe-0/0/2:2
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-junos-interfaces
```

## License

BSD 2-clause
---
